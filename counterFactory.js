const counterFactory = function () {
    let count = 0;
    return {
        incrementFunction() {
            count++;
            return count;
        },
        decrementFunction() {
            count--;
            return count;

        }
    }
}
module.exports = counterFactory;