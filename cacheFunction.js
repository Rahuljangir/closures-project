const cacheFunction = function (callBack) {
    const cache = {};
    function returnFunction(key) {
        
        if (key in cache){
            return cache;
        }
        else{
            cache[key] = callBack(key);
        }
        return cache[key];
     
    }
    return returnFunction;
}

module.exports = cacheFunction;