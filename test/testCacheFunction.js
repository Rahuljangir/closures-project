let cacheFunction = require('../cacheFunction.js');
const callBack = (value) => value += 1 ;

const cache = cacheFunction(callBack);
console.log(cache("hello"));
console.log(cache("Rahul"));
console.log(cache("hello"));