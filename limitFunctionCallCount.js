const limitFunctionCallCount = function (callBack, num) {
    let count = 10;
    function returnFunction() {
        for(let index = 0;index < num; index++){  
          count = callBack(count) ;
        } 
        if(!count){
          return null;
        }
        else{
          return count;
        }
    }
    
    return returnFunction();
}

module.exports = limitFunctionCallCount;